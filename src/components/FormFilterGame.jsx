import React, {useContext} from 'react'
import { Form, Select, InputNumber, Button, Checkbox, Row, Col, Input } from 'antd';
import {Context} from '../store'
import Axios from 'axios'

const { Option } = Select;

const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 14 },
};
const formSearchLayout = {
  wrapperCol: { span: 20 },
};

const FormFilterGame = () => {
  const [,,, setGames,,, action] = useContext(Context)
  const [form] = Form.useForm();

  const reset = () => {
    form.resetFields()
    action.fetchGames()
  }

  const onFinish = value => {
    Axios.get(`https://backendexample.sanbersy.com/api/data-game`)
    .then(({data}) => {
      let result = [...data]
      if(value.genre){
        result = result.filter(data => (String(data.genre)).toLowerCase().indexOf(value.genre.toLowerCase()) !== -1)
      }
      if(value.platform){
        console.log(typeof(value.platform));
        result = result.filter(data => (String(data.platform)).toLowerCase().indexOf(value.platform.toLowerCase()) !== -1)
      }
      if(value.release){
        result = result.filter(data => Number(data.release) === Number(value.release))
      }
      if(value.mode){
        if(value.mode.length === 1){
          result = result.filter(data => data[value.mode[0]] === 1)
        } else if(value.mode.length === 2){
          result = result.filter(data => data[value.mode[0]] === 1 && data[value.mode[1]] === 1)
        }
      }
      setGames(result);
    }).catch((err) => {
      console.log(err);
    })
    
  }

  const searchGame = ({keySearch}) => {
    Axios.get(`https://backendexample.sanbersy.com/api/data-game`)
    .then(({data}) => {
      if(keySearch){
        let result = data.filter((val) => (String(val.name).toLowerCase()).indexOf(keySearch.toLowerCase()) !== -1)
        setGames(result)
      } else {
        setGames(data)
      }
    }).catch((err) => {
      console.log(err);
    })
  }

  return (
    <div>
      <br />
      <h3>Search by Name:</h3>
      <Form
        name="search-form"
        {...formSearchLayout}
        onFinish={searchGame}
      >
        <Form.Item name="keySearch">
          <Input />
        </Form.Item>
        <Form.Item wrapperCol={{ span: 14, offset: 7 }}>
          <Button type="primary" htmlType="submit">
            Search
          </Button>
        </Form.Item>
      </Form>
      <br />
      <h2>Filter:</h2>

      <Form
        form={form}
        name="nest-messages"
        {...formItemLayout}
        onFinish={onFinish}
      >
        <Form.Item name="genre" label="Genre">
          <Select placeholder="Select game genre">
            <Option value="Action">Action</Option>
            <Option value="War">War</Option>
            <Option value="Horror">Horror</Option>
            <Option value="Survival">Survival</Option>
            <Option value="Fantacy">Fantacy</Option>
            <Option value="Thriller">Thriller</Option>
            <Option value="Fiction">Fiction</Option>
            <Option value="Non-Fiction">Non-Fiction</Option>
            <Option value="Education">Education</Option>
            <Option value="History">History</Option>
            <Option value="Stealth">Stealth</Option>
            <Option value="Comedy">Comedy</Option>
            <Option value="Mistery">Mistery</Option>
            <Option value="Romance">Romance</Option>
          </Select>
        </Form.Item>

        <Form.Item name="platform" label="Platform" >
          <Select placeholder="select game platform">
            <Option value="windows">Windows</Option>
            <Option value="playstation">Playstation</Option>
            <Option value="xbox">Xbox</Option>
            <Option value="nintendo">Nintendo</Option>
            <Option value="android">Android</Option>
            <Option value="iOS">IOS</Option>
          </Select>
        </Form.Item>

        <Form.Item label="Year" rules={[{type: 'number'}]}>
          <Form.Item name="release" noStyle>
            <InputNumber max={2020} min={1990} type='number' />
          </Form.Item>
        </Form.Item>

        <Form.Item name="mode" label="Mode">
          <Checkbox.Group>
            <Row>
              <Col>
                <Checkbox value="singlePlayer" style={{ lineHeight: '32px' }}>
                  Single Player
                </Checkbox>
              </Col>
              <Col>
                <Checkbox value="multiplayer" style={{ lineHeight: '32px' }}>
                  MultiPlayer
                </Checkbox>
              </Col>
            </Row>
          </Checkbox.Group>
        </Form.Item>

        <Form.Item wrapperCol={{ span: 14, offset: 4 }}>
          <Row justify='space-around'>
            <Col>
              <Button type="primary" htmlType="submit" >
                Submit
              </Button>
            </Col>
            <Col>
              <Button type="primary" onClick={reset} >
                Reset
              </Button>
            </Col>

          </Row>
        </Form.Item>
      </Form>
    </div>
  );
};

export default FormFilterGame