import React from 'react'
import { Layout } from 'antd';
const { Footer } = Layout;

class FooterComp extends React.Component {
  render(){
    return (
      <Footer style={{ textAlign: 'center' }}>Muhammad Fauzaan Irsyaadi ©2020 Layout by Ant UED</Footer>
    )
  }
}

export default FooterComp
