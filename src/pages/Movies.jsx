import React, {useContext, useEffect} from 'react'
import {Row} from 'antd'
import MoviesCard from '../components/MoviesCard'
import {Context} from '../store'

const Movies = () => {
  const [,,,,movies,,action] = useContext(Context)
  useEffect(() => {
    action.fetchMovies()
  }, [])

  return (
    <div>
      <h1>Movies</h1>
      <Row>
        {
          movies &&
          movies.map((val, i) => {
            return (<MoviesCard key={i} data={val} />)
          })
        }

      </Row>
    </div>
  )
}

export default Movies
