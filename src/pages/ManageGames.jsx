import React, {useEffect, useContext} from 'react'
import { Link, useHistory } from 'react-router-dom'
import Axios from 'axios'
import { Table, Image, Button, message, Row, Col } from 'antd';
import { CloseCircleOutlined, CheckCircleTwoTone, FileAddOutlined } from '@ant-design/icons';
import {Context} from '../store'
import FormFilterGame from '../components/FormFilterGame';

const ManageGames = () => {
  const [user,,games,,,,action] = useContext(Context)
  const history = useHistory()
  
  useEffect(() => {
      if(user.name){
        action.fetchGames()
      } else {
        history.push('/login')
      }
  }, [])
  
  const handleDelete = (ID_GAMES) => {
    Axios.delete(`https://backendexample.sanbersy.com/api/data-game/${ID_GAMES}`, {
      headers: {
        Authorization: `Bearer ${user.token}`
      }
    })
    .then(() => {
      message.success('Delete Data Success')
      action.fetchGames()
    }).catch((err) => {
      console.log(err.response);
      message.error('Delete Data Failed1')
    });
  }

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      sorter: (a, b) => Number(a.id) - Number(b.id),
    },
  
    {
      title: 'Poster Image',
      dataIndex: 'image_url',
      render: (i) => {
        return <Image width={100} src={i}/>
      }
    },
  
    {
      title: 'Name',
      dataIndex: 'name',
      sorter: (a, b) => {
        if(a.name < b.name) { return -1; }
        if(a.name > b.name) { return 1; }
        return 0;},
    },
  
    {
      title: 'Genre',
      dataIndex: 'genre',
      sorter: (a, b) => {
        if(a.name < b.name) { return -1; }
        if(a.name > b.name) { return 1; }
        return 0;}
    },
  
    {
      title: 'Platform',
      dataIndex: 'platform',
      sorter: (a, b) => {
        if(a.name < b.name) { return -1; }
        if(a.name > b.name) { return 1; }
        return 0;}
    },
  
    {
      title: 'Release',
      dataIndex: 'release',
      sorter: (a, b) => Number(a.release) - Number(b.release),
    },
  
    {
      title: 'Single Player',
      dataIndex: 'singlePlayer',
      sorter: (a, b) => Number(a.singlePlayer) - Number(b.singlePlayer),
      render: (val) => {
        return (
          Number(val) === 1
          ? <CheckCircleTwoTone twoToneColor="#52c41a" />
          : <CloseCircleOutlined style={{opacity: '50%'}} />
        )
      }
    },
  
    {
      title: 'Multi Player',
      dataIndex: 'multiplayer',
      sorter: (a, b) => Number(a.multiplayer) - Number(b.multiplayer),
      render: (val) => {
        return (
          Number(val) === 1
          ? <CheckCircleTwoTone twoToneColor="#52c41a" />
          : <CloseCircleOutlined style={{opacity: '50%'}} />
        )
      }
    },
  
    {
      title: 'Action',
      dataIndex: 'id',
      key: 'x',
      render: (id) => (
        <>
          <Link to={`/games/${id}`}>Detail</Link><br />
          <Link to={`/games/edit/${id}`}>Edit</Link><br />
          <a onClick={() => handleDelete(id)}>Delete</a>
        </>
      ),
  
    },
  ];

  return (
    <div>
    <h1>Manage Games</h1>
      <Row justify='end'>
        <Link to='/games/create' style={{float: "right", margin: '10px'}} >
          <Button type="primary" icon={<FileAddOutlined />}>
            Add New Game
          </Button>
        </Link>
      </Row>
      <Row>
        <Col span={4}>
          <FormFilterGame />
        </Col>
        <Col span={20}>
          <Table columns={columns} dataSource={games}/>
        </Col>
      </Row>
    </div>
  )
}

export default ManageGames
