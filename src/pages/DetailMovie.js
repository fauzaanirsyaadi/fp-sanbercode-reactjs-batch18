import React, {useEffect, useState} from 'react'
import Axios from 'axios'
import {useParams} from 'react-router-dom'
import {Row, Col, Image} from 'antd'

const DetailMovie = () => {
  const [movie, setMovie] = useState({})
  let {id} = useParams()
  
  useEffect(() => {
    Axios.get(`https://backendexample.sanbersy.com/api/data-movie/${id}`)
    .then(({data}) => {
      setMovie(data)
    }).catch((err) => {
      console.log(err.response);
    });
  }, [id])
  return (
    <Row style={{backgroundColor: 'aqua', padding: '30px'}}>
      <Col span={7} style={{display: 'flex', justifyContent: 'center'}}>
        <Image src={movie.image_url}/>
      </Col>
      <Col span={17} style={{paddingLeft: '30px'}}>
        <h1 style={{marginBottom: '0px'}}>{movie.title} ({movie.year})</h1>
        <p>{movie.genre} - {movie.duration} minutes</p>
        <h4>Rating: {movie.rating}</h4>
        <h3>Description:</h3>
        <p>{movie.description}</p>
        <h3>Review:</h3>
        <p>{movie.review}</p>
      </Col>
    </Row>
  )
}

export default DetailMovie
